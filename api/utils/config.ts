import dotenv from 'dotenv'

dotenv.config()

const Config = {
    'CONTENTFUL_SPACE_ID': process.env.CONTENTFUL_SPACE_ID,
    'CONTENTFUL_TOKEN': process.env.CONTENTFUL_TOKEN
}

export { Config }