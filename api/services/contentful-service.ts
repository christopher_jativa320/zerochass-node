import { Config } from '@utils/config'

const contentful = require('contentful')

const client = contentful.createClient({
    space: Config.CONTENTFUL_SPACE_ID,
    accessToken: Config.CONTENTFUL_TOKEN
})

export const getTutorial = async (request, response) => {
    console.log('The url parameter is', request.params.slug)
    response.json('Hey there')
    // let tutorial = await client.getEntry(request.params.slug)
    // console.log(tutorial.fields.title)
}

export const getTutorials = async (request, response) => {

    let entries = (await client.getEntries()).items
    response.json(entries)
}