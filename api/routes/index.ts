import express from 'express'
import jwt from 'express-jwt'

import * as ContentfulService from '@services/contentful-service'

const router = express.Router()

router.get('/tutorial/:slug', ContentfulService.getTutorial)
router.get('/tutorials', ContentfulService.getTutorials)

export { router }