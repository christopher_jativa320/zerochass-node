import express from 'express'
import path from 'path'
import bodyParser from 'body-parser'

import { router } from '@routes/index'
const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use(express.static(path.join(__dirname, 'dist')))
app.use('/api', router)
app.use('/chris', (request, response) => {
    response.json('Hey Chris')
})

app.use('*', (request, response) => {
    response.sendFile(path.join(__dirname, 'dist/index.html'));
})

const server = app.listen(8081, () => {
    let port = server.address();
    console.log('Server running at ' , port);
});