const path = require('path');

module.exports = {
    entry: './app.ts',
    target: 'node',
    devtool: 'inline-source-map',
    node: {
        __dirname: false,
    },
    module: {
        rules: [
            {
                test: /\.ts?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
        alias: {
            '@api': path.resolve(__dirname, 'api/'),
            '@routes': path.resolve(__dirname, 'api/routes/'),
            '@services': path.resolve(__dirname, 'api/services/'),
            '@utils': path.resolve(__dirname, 'api/utils/'),
        }
    },
    output: {
        filename: 'app.js',
        path: __dirname
    },
};